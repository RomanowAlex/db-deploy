## Настройка
* vagrant up / halt / status
* admin@example.com -> new password
* create group: db-increment, project: db-increment
* 127.0.0.1 gitlab.local >> hosts (windows)
* configure ssh
* generate ssh (http://gitlab.local/help/ssh/README#generating-a-new-ssh-key-pair)
* init git repository
* register runner
* http://192.168.50.5/students/db-increment/settings/ci_cd
* https://docs.gitlab.com/runner/register/index.html (shell runner)
* git remote add origin git@gitlab.local:students/db-increment.git
* https://docs.gitlab.com/ee/ci/ssh_keys/#ssh-keys-when-using-the-shell-executor
* gitlab runner: ~/.ssh generate ssh-keygen
* database: GL id_rsa.pub > DB ~/.ssh/authorized_keys