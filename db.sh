#!/bin/bash

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install htop fish -y
sudo add-apt-repository 'deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main'
sudo add-apt-repository ppa:webupd8team/java -y
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install postgresql-10 -y
sudo -u postgres psql -c "create role testuser with login password 'test';"
sudo -u postgres psql -c "create database testdb;"
sudo -u postgres psql -c "grant all privileges on database testdb to testuser;"

sudo useradd -m -G sudo deploy
sudo -u deploy mkdir ~/.ssh 
sudo -u deploy chmod 700 ~/.ssh
sudo -u deploy touch ~/.ssh/authorized_keys
sudo -u deploy chmod 644 ~/.ssh/authorized_keys

echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | sudo debconf-set-selections
sudo apt-get install oracle-java8-installer -y
sudo apt install oracle-java8-set-default

sudo mkdir -p /usr/local/lib/liquidbase
cd /usr/local/lib/liquidbase
sudo chown vagrant:vagrant /usr/local/lib/liquidbase
sudo chmod 755 /usr/local/lib/liquidbase
wget https://github.com/liquibase/liquibase/releases/download/liquibase-parent-3.6.1/liquibase-3.6.1-bin.tar.gz
tar zxvf liquibase-3.6.1-bin.tar.gz
rm liquibase-3.6.1-bin.tar.gz
sudo ln -s /usr/local/lib/liquidbase/liquibase /usr/local/bin
