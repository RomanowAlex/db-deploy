#!/bin/bash

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y htop fish curl openssh-server ca-certificates
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
sudo sh -c 'echo "127.0.0.1 gitlab.local" >> /etc/hosts'
sudo EXTERNAL_URL="http://gitlab.local" apt-get install gitlab-ce

sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo useradd --create-home gitlab-runner --shell /bin/bash
sudo -u gitlab-runner mkdir ~/.ssh 
sudo -u gitlab-runner chmod 700 ~/.ssh
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start